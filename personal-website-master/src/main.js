function createDynamicElements() {
    const container = document.getElementById("container");
  
    const titleElement = document.createElement("h1");
    titleElement.textContent = "Join Our Program";
    container.appendChild(titleElement);
  
    const textElement = document.createElement("p1");
    textElement.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
    container.appendChild(textElement);
  
    const searchBar = document.createElement("div");
    searchBar.classList.add("search-bar");
    container.appendChild(searchBar);
  
    const emailInput = document.createElement("input");
    emailInput.type = "email";
    emailInput.placeholder = "Email";
    emailInput.required = true;
    searchBar.appendChild(emailInput);
  
    const subscribeButton = document.createElement("button");
    subscribeButton.textContent = "SUBSCRIBE";
    searchBar.appendChild(subscribeButton);
  
    const joinForm = document.getElementById("joinForm");
    joinForm.addEventListener("submit", function (event) {
      event.preventDefault(); 
  
      const enteredEmail = emailInput.value;
      console.log("Entered email:", enteredEmail);
    });
  
    subscribeButton.addEventListener("click", function () {
      emailInput.value = ""; 
    });
  }
  
  createDynamicElements();
  